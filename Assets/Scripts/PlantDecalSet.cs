﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlantDecalSet : ScriptableObject
{
    public GameObject[] bases;
    public GameObject[] flowers;
    public GameObject[] fruits;


}
